mswg: Modular Static Website Generator
(sorry if anything is bad its my first readme >///<)


mswg works by replacing components in templates to create webpages and 
creating directory pages. Templates are html documents that have component
"slots" (still working on naming alot of things in mswg). The goal of 
mswg is to provide a simple framework for simple static website creation with
the added benefit of being super modular and easy to tweak. Porting mswg to any language with file i/o should be really simple.


Definitons (set to change as I come up for better names for things):

Templates are valid HTML documents with at least 2 @@slots@@: @@main@@ and
@@page_name@@. Located in the templates/ directory.

"Documents" are files that can contain plaintext, HTML, and slots. During Phase 1 they are used to create HTML files. "Documents" replace the @@main@@ "slot"and use a substring of the its filename ending right before the last period.To specify the template desired append a psuedo file extention (ie "test.test_temp" would use "test_temp" found in templates/ directory). If no template is specified, then "default" will be used.

Components are snippets of HTML. They can either be static or "compiled".
Static components are stored in the components/ directory. "Compiled" components are executables (scripts or binaries) that will create HTML  during Phase 2. The "Compiled" component executables are placed in armed_components/ and the output HTML snippets are stored in the components/ directory.

"Slots" are placeholders for HTML snippets called to be placed in Phase 3. "Slots" are denoted like @@this@@. The string inside the double @ symbols will be the filename for the component in components/. @@test@@ "slot" will be filled by components/test.


mswg site creation happens in 3 phases:

Phase 1: replacing @@main@@ and @@page_name@@ "slots" in templates, moving the resultant HTML files into the rendered/ directory and creating HTML directory pages
Phase 2: "compiling" armed_components
Phase 3: replacing components in the html files in rendered


Phase 1:
During phase 1, "documents" in the source/ directory are used to fill the
@@main@@ and @@page_name@@ "slots" found in templates and create directory webpages.

During phase 1, when supplied with the "document" "test.test_template" 
mswg would:
1. retrieve test_template from templates/test_template
2. replace the @@main@@ "slot" in test_template with the contents of test.test_template
3. replace @@page_name@@ "slot" with the substring of the filename that ends before the last period. In this case, "test".
4. output into file "test.html"
5. move "test.html" into the rendered directory

During phase 1, when supplied with the directory "test_directory" mswg would:
1. create "document" "test_directory.dir"
2. append <a> tags to the "document". linking a substring of the filename ending before the first period with ".html" appended. For example, "test.test_temp" would be linked with <a href="test.html">test</a>
3. replace @@main@@ and @@page_name@@ in a "dir" template found in templates/
If a "dir" template can't be found, "default" will be used instead.
4. move the new .HTML file into rendered/
5. delete the .dir "document"
6. if any directories are found, repeat the process for them.


Phase 2:
During phase 2, "compiled" components will be made. All executables within the armed_components/ directory will be ran and their resultant components will be placed in the component/ directory.

"Compiled" components can be written in any language. If it is a compiled language, place the binary in the armed_components/ directory. If it is an interpreted language, make sure the script will be interpreted, either by putting the interpreter in $PATH or using a shebang.

Phase 3:
During phase 3, all "slots" will be replaced by components. For example the "slot" "@@test@@" will be replaced by the contents of the file components/test.


As you can see, mswg is more of a protocol than an actual program. That's why I will be porting it to C as soon as I'm more comfortable with C. But I've tried Scheme recently and it feels super nice to work with. For now its just a very modular website generator.
